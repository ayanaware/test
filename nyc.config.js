module.exports = {
	include: [
		'lib/**/*.ts',
		'src/**/*.ts'
	],
	exclude: [
		'lib/**/*.spec.ts',
		'src/**/*.spec.ts'
	],
	extension: [
		'.ts'
	],
	all: true,
};
