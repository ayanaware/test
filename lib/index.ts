import 'mocha';
import * as sinon from 'sinon';

import autoDescribe, { AutoSuiteFunction } from './autoDescribe';
import instance from './expect';

(global as any).sinon = sinon;
(global as any).expect = instance;
(global as any).autoDescribe = autoDescribe;

declare global {
	export const sinon: sinon.SinonStatic;
	export function expect(subject: any, assertion: string, ...args: Array<any>): void; // TODO: Maybe make some typings
	export const autoDescribe: AutoSuiteFunction;
}
