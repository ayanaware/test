import { ExclusiveSuiteFunction, PendingSuiteFunction, Suite } from 'mocha';

export interface AutoSuiteFunction {
	/**
	 * [bdd, tdd] Describe a "suite" with an automatically generated title from the filename
	 * and callback `fn` containing nested suites.
	 *
	 * - _Only available when invoked via the mocha CLI._
	 */
	(fn: (this: Suite) => void): Suite;

	/**
	 * [qunit] Describe a "suite" with an automatically generated title from the filename.
	 *
	 * - _Only available when invoked via the mocha CLI._
	 */
	(): Suite;

	/**
	 * [bdd, tdd, qunit] Indicates this suite should be executed exclusively.
	 *
	 * - _Only available when invoked via the mocha CLI._
	 */
	only: ExclusiveSuiteFunction;

	/**
	 * [bdd, tdd] Indicates this suite should not be executed.
	 *
	 * - _Only available when invoked via the mocha CLI._
	 */
	skip: PendingSuiteFunction;
}

const describe = global.describe;

const autoDescribe = function(fn: (this: Suite) => void) {
	const pst = Error.prepareStackTrace;
	Error.prepareStackTrace = (e, stk) => {
		Error.prepareStackTrace = pst;

		return stk;
	};

	const stack: Array<NodeJS.CallSite> = new Error().stack as any;
	const fileName = stack[1].getFileName();

	const results = /\/(?:lib|src)\/(.*)$/g.exec(fileName);

	return describe(`${results[1] || 'Auto-Describe failed'}`, fn);
};

autoDescribe.skip = describe.skip;
autoDescribe.only = describe.only;

export default autoDescribe as AutoSuiteFunction;
