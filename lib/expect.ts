const unexpected = require('unexpected');
const instance = unexpected.clone();

instance.addAssertion(
	'<any> [not] to be an array',
	function(expect: any, subject: any) {
		expect(Array.isArray(subject), '[not] to be true');
	}
);

instance.addAssertion(
	'<object> [not] to be an instance of <function>',
	function(expect: any, subject: object, fn: Function) {
		expect(subject instanceof fn, '[not] to be true');
	}
);

export default instance as 'expect';
