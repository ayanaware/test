import './index';
import { TestInjector as DiTestInjector } from './TestInjector';

try {
	(global as any).TestInjector = require('./TestInjector').TestInjector;
} catch (e) {
	// Tell the user that something is wrong
	const throwError = () => { throw new Error('TestInjector is only available if @ayana/di is installed!'); };
	(global as any).TestInjector = new Proxy({}, {
		construct: throwError,
		get: throwError,
		set: throwError,
		apply: throwError,
	});
}

declare global {
	// tslint:disable-next-line: variable-name
	export const TestInjector: typeof DiTestInjector;
}
