import { Injector, isType, Type } from '@ayana/di';

export class TestInjector<T> extends Injector {
	private readonly testedType: Type<T>;
	private readonly stubbed: Set<Type<any>> = new Set();

	private testing: boolean = false;

	public constructor(testedType: Type<T>) {
		super();

		this.testedType = testedType;
	}

	public static test<T>(type: Type<T>): TestInjector<T> {
		const injector = new TestInjector(type);
		injector.test(type);

		return injector;
	}

	public test(type: Type<T>): T {
		if (this.testing) throw new Error('Already testing. Cannot run the test method twice on the same Injector');

		this.runHandlePendingRecords = false;

		this.provide({ provide: type, useClass: type });

		for (const dep of this.getRecord(type).deps) {
			// If the dependency is not a type or the token is known we can't stub it
			// In this case the user has to provide the stub itself
			if (!isType(dep.token) || this.knownToken(dep.token)) continue;

			this.provide({ provide: dep.token, useValue: sinon.createStubInstance(dep.token) });
			this.stubbed.add(dep.token);
		}

		this.runHandlePendingRecords = true;

		this.handlePendingRecords();
		this.verify();

		const value = this.get(type);
		if (value == null) throw new Error(`Value for tested object token ${this.stringifyToken(type)} is null`);

		this.testing = true;

		return value;
	}

	public getTested(): T {
		return this.get(this.testedType);
	}

	public getStub<S>(token: Type<S>): sinon.SinonStubbedInstance<S> {
		if (!this.stubbed.has(token)) throw new Error(`${this.stringifyToken(token)} is not stubbed for this TestInjector`);

		return this.get(token) as sinon.SinonStubbedInstance<S>;
	}
}
