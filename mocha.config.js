module.exports = {
	bail: true,
	fullTrace: true,
	spec: [
		'?(lib|src)/**/*.spec.ts'
	],
	require: [
		'ts-node/register'
	],
};
