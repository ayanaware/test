@ayana/test [![NPM](https://img.shields.io/npm/v/@ayana/test.svg)](https://www.npmjs.com/package/@ayana/test) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)
===

## Installation

With NPM

```
npm i @ayana/test typescript --save-dev
```

With Yarn

```
yarn add @ayana/test typescript --dev
```


## Basic Usage

1. Add the following scripts to your `package.json`:

    ```json
    {
	    "name": "your-project",
	    ...
	    "scripts": {
			"test": "ayt test",
			"cover": "ayt coverage"
	    }
    }
    ```

    By Ayana convention the code lives in the `src` directory if it's an application and in the `lib` directory if it's a library.
	Files outside of these directories will be ignored.

2. Exclude `.spec.ts` files in your `tsconfig.json` (If you are using `ayc` from [@ayana/ts](https://www.npmjs.com/package/@ayana/ts) this step can be skipped):

    ```json
    {
	    ...
	    "exclude": [
		    "./lib/**/*.spec.ts"
	    ]
    }
    ```

3. Create `.spec.ts` files named after the file you want to test next to the file you want to test:

    Say your file lives under `lib/hello/Hello.ts`, then you would create a file for the tests at `lib/hello/Hello.spec.ts`.

    ```ts
    import '@ayana/test';

    autoDescribe(function() { // autoDescribe will take the path after the lib or src folder and use it as the descriptor
	    describe('#constructor', function() {
		    it('should do a thing', function() {

		    });
	    });
    });
    ```

    Tests are written with `mocha`. In addition there's also the global variables `sinon` and `expect` (powered by unexpected) available for usage in tests.

## Testing with [@ayana/di](https://www.npmjs.com/package/@ayana/di)

When using the import `@ayana/test/di` this package adds another global variable called `TestInjector`. Using this you can easily write Unit-Tests for injectables and the `TestInjector` will automatically create a sinon stub for all required injectables.

### Tested.spec.ts:
```ts
import '@ayana/test/di';

import { Tested } from './Tested';

import { SomeDependency } from './SomeDependency';

autoDescribe(function() {
	describe('#doSomething', function() {
		it('should do a thing', function() {
			const injector = TestInjector.test(Tested);
			const tested = injector.getTested();

			tested.doSomething();

			expect(
				injector.getStub(SomeDependency).someMethod.callCount,
				'to be',
				1
			);
		});
	});
});
```

### Tested.ts:
```ts
import { Inject } from '@ayana/di';

import { SomeDependency } from './SomeDependency';

export class Tested {
	constructor(@Inject() private someDependency: SomeDependency) {}

	doSomething() {
		this.someDependency.someMethod();
	}
}
```

### SomeDependency.ts:
```ts
export class SomeDependency {
	someMethod() {
		console.log('Does Something');
	}
}
```

## Troubleshooting

### I'm getting errors because typings could not be found but the regular tsc command works

This might be caused by you having custom `.d.ts` files in your project. `ts-node` apperantly resolves custom `.d.ts` files differently
than `tsc` does.

You can find a way to fix this in the [TS-Node README](https://github.com/TypeStrong/ts-node#help-my-types-are-missing). You want to set the `baseUrl` and the `paths` array in the `compilerOptions` in your `tsconfig.json` like it's mentioned there.

### I'm getting errors about experimental decorator support but I have it enabled in my tsconfig.json

If you get an error like this:

`Experimental support for decorators is a feature that is subject to change in a future release. Set the 'experimentalDecorators' option in your 'tsconfig' or 'jsconfig' to remove this warning. ts(1219)`

It is because you excluded the `.spec.ts` in your main `tsconfig.json` like mentioned above so that config doesn't apply to `.spec.ts` files anymore.
If you need to use decorators in your tests, then you currently have to create a second file for the actual build (`tsconfig.prod.json` is recommended) and move the exclude property for the `.spec.ts` files there:

```json
{
	"extends": "./tsconfig.json",
	"exclude": [
		"./lib/**/*.spec.ts"
	]
}
```

Also you need to update your build command to `tsc -p tsconfig.prod.json`. Running `tsc` should work just fine, however the output directory will also contain the compiled files of the tests which you might not want in your npm package or deployed application.
